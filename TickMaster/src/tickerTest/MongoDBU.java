
/**
 * The conversion of the Liquid Metrics SQL-Server MongoBridge package
 */
package tickerTest;

import java.util.ArrayList;
import java.util.regex.Pattern;

import com.mongodb.DBCursor;
import com.gmanalytics.symbology.persistence.impl.mongoDriver.MongoDba.MongoDriverUtils;

/**
 * @author MarkBosa - MongoDB Utilities used to get a Java MongoDB Driver 
 *                    connection.  Currently  there is a dependency on the
 *                    symbology mongoDriver implementation, but this can be
 *                    re-factored into it's own project is if needs to be 
 *                    shared with other projects.
 * 
 */


public class MongoDBU {
	public static final int LOCALHOST = 1;
	public static final int TERMINAL_SERVER = 2;
	public static final int TEST_CLUSTER_R1 =3;
	public static final int TEST_CLUSTER_R2 = 4;
	MongoDriverUtils mdu = null;
	Pattern pattern = null;
	DBCursor globalCursor = null;
	ArrayList<String> resultList = new ArrayList<String>();

	/**
	 * Get a mongoDB connection and set this.mdu
	 */
	public MongoDBU() { 
		this.mdu = new MongoDriverUtils();
	}
	
	/**
	 * Call immediately after creating this class
	 * @return true if connection is successful
	 */
	public boolean MongoInitialize(int testTarget) {
		/**
		 * Set (or over-ride) the Mongo-Driver connection parameters as 
		 * needed to connect to MongoDB target 
		 */
		switch(testTarget) {
		case LOCALHOST:						// Local laptop mongos instance
			mdu.setPortNum(27017);
			mdu.setHostName("localhost");   
			mdu.setDbName("tickMan");
			break;
			
		case TERMINAL_SERVER :				// mongod.exe on Terminal Server
			mdu.setPortNum(27017);
			mdu.setHostName("10.27.2.9");
			mdu.setDbName("tickMan");
			break;
			
		case TEST_CLUSTER_R1:				// MongoConf-27-1: mongos instance
			mdu.setPortNum(27017);
			mdu.setHostName("10.27.2.221"); 
			mdu.setDbName("tickMan");
			break;
			
		case TEST_CLUSTER_R2:				// MongoTS: port for mongos.exe instance
			mdu.setPortNum(27017);			
			mdu.setHostName("10.27.2.214");   
			mdu.setDbName("tickMan");
			break;
		default:		
			mdu.setPortNum(27017);
			mdu.setHostName("localhost");   
			mdu.setDbName("tickMan");
			break;
		}
		/**
		 * Now instantiate a single MongoDB client connection
		 * This has the side-effect of setting the two key instance variables
		 * "mongoClient" and the "this.connection" used by Search Type Queries 
		 */
		Boolean mrc = mdu.createMongoClient();
	
		System.out.println("Executive main()--> " +
				((mrc == true) ? " Successfully Connected" : "Failed to Connect ")
				+ " to MongoDB" 
				+ " Database:"   + mdu.getDbName() 
//				+ " Collection:" + di.getDBCollection().toString()
				+ " on Host:"    + mdu.getHostName() 
				+ " on Port:"    + mdu.getPortNum());

		return mrc;	
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/**
		 * Step 1: Instantiate an instance of this class
		 */
		MongoDBU mdbu = new MongoDBU();
		/**
		 * Step 2: Procure and Initialize the MongoDB Connection
		 */
		boolean rc = mdbu.MongoInitialize(MongoDBU.LOCALHOST);
		
		if(rc == false) {
			System.out.println("Mongo Failed to Initialize!");
			System.exit(-1);
		}
		else {
			System.out.println("MongoDB Initialized Sucessfully");
		}
	}
}

