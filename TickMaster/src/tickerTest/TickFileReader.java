/**
 * 
 */
package tickerTest;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;

import com.gmanalytics.algorithms.io.CSVParserBuilder;
import com.gmanalytics.algorithms.io.CSVReader;
import com.gmanalytics.algorithms.io.CSVReader.LineTooLongException;
import com.gmanalytics.algorithms.io.CSVReader.ParseErrorException;
import com.mongodb.DBObject;

/**
 * @author MarkBosa
 *
 */
public class TickFileReader {
	private Reader tickReader = null;
	private long lineCounter = 0;
	private String csvFileName = null;

	/**
	 * Default Constructor
	 */
	public TickFileReader() {
	}	

	/**
	 * Getter and Setter Methods
	 */
	public Reader getTickReader() {
		return tickReader;
	}

	public void setTickReader(Reader tickReader) {
		this.tickReader = tickReader;
	}
	
	public long getLineCounter() {
		return lineCounter;
	}

	public void setLineCounter(long lineCounter) {
		this.lineCounter = lineCounter;
	}

	public String getCsvFileName() {
		return csvFileName;
	}

	public void setCsvFileName(String csvFileName) {
		this.csvFileName = csvFileName;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final int SINGLE_INSERT = 1;
		final int SIMPLE_PRODUCER_CONSUMER = 2;
		int mode = SIMPLE_PRODUCER_CONSUMER;
		
		/**
		 *  Step One: Identify and open reader for tick data file
		 *  This is the input source of data
		 */
		TickFileReader tfr = new TickFileReader();
		tfr.setCsvFileName("C:\\Dev\\ClusterTest\\data\\tickData.txt");
		
		/**
		 * Step Two: Set up a MongoDB Connection which will be the persistent
		 * data store for the tick data.   Note that we use a wrapper class
		 * MongoDBU to specify the database name, port number, etc.
		 */
		MongoDBU mdbu = new MongoDBU();
		mdbu.MongoInitialize(MongoDBU.TERMINAL_SERVER);
		MongoTickDAO mtDao = new MongoTickDAO(mdbu.mdu);
		mtDao.setMongoTickDao(mtDao);
        Date t1 = new Date();

		Reader reader;
		try {
			reader = new FileReader(tfr.getCsvFileName());
			//		    int data = reader.read();
			//		    int max = 0;
			//		    while(data != -1){
			//		        char dataChar = (char) data;
			//		        data = reader.read();
			////		        System.out.println("dataChar:" + dataChar + " data=[" + data + "]");
			//		        System.out.print(dataChar);
			//
			//		        if(max++ > 100)
			//		        	System.exit(1);
			//		    }

			/**
			 * Step 3: Instantiate a TickFileReader class with the input file
			 */	
			tfr.setTickReader(reader);

			CSVParserBuilder csvParserBuilder = new CSVParserBuilder();
			csvParserBuilder.addDelimiter('\t');  // tab separated data

			CSVReader csvr = new CSVReader(tfr.getTickReader(), csvParserBuilder);
			String[] nextLine = null;
			DBObject tickDoc = null;
			
			nextLine = csvr.read();

			/**
			 * Step Four: Read one line at a time from the reader and then
			 * store that line as a new MongoDB document
			 */
			while( nextLine != null) {

//				System.out.println(tfr.lineCounter++ +"::");
//
//				for(int i = 0; i < nextLine.length; i++) {
//					//System.out.println("Token(" + i + ")=[" + nextLine[i] + "]");
//					System.out.print( nextLine[i] + '\t');
//					
//				}
 				if(++tfr.lineCounter % 1000000 == 0) {
					Date t2 = new Date();
				       long millis = t2.getTime() - t1.getTime();
				       long seconds = (millis / 1000) % 60;
				       long minutes = ((millis - seconds) / 1000) / 60;
				       
				       System.out.printf(
				    	"TickFileReader::Inserted %d Docs in %d minutes, %d seconds %d millis \n", 
				    		tfr.lineCounter, minutes, seconds, millis);
				       
				       /**
				        * Run the garbage collector every million objects as 
				        * import is in the queue and we should be able to 
				        * reclaim some temporary objects 
				        */
				       System.gc();
 				}

                tickDoc = mtDao.createTickDoc(nextLine);
                
                switch(mode) {
                case SINGLE_INSERT :
                	mtDao.InsertTickDocument(tickDoc);
                	break;
                	
                case SIMPLE_PRODUCER_CONSUMER:
                	mtDao.AddToMessageQueue(tickDoc);
                	break;
                }
				nextLine = csvr.read();
			}

		} catch (FileNotFoundException e) {
			System.out.println( "FileNotFoundException at line::" + tfr.getLineCounter());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println( "IOException at line::" + tfr.getLineCounter());
			e.printStackTrace();
		} catch (ParseErrorException e) {
			System.out.println( "ParseErrorException at line::" + tfr.getLineCounter());
			e.printStackTrace();
		} catch (LineTooLongException e) {
			System.out.println( "LineTooLongException at line::" + tfr.getLineCounter());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println( "InterrruptedException at line::" + tfr.getLineCounter());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println( "Exception at line::" + tfr.getLineCounter());
			e.printStackTrace();
		}
	
	   Date t3 = new Date();
       long millis = t3.getTime() - t1.getTime();
       long seconds = (millis / 1000) % 60;
       long minutes = ((millis - seconds) / 1000) / 60;
       System.out.printf("TickFileReader:: Insert duration: %d minutes, %d seconds %d millis\n",
               minutes, seconds, millis);
       System.out.println("TickFileReader:: Inserted " + tfr.lineCounter 
    		   				+ " Documents into MongoDB TickMaster.ticks collection\n");
       System.out.println("Maximum onConsume() size=" + mtDao.getMaxOnConsumeSize());
	}
}
