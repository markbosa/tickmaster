package tickerTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;

import com.gmanalytics.algorithms.concurrent.SimpleProducerConsumer;
import com.gmanalytics.algorithms.structures.CircularArrayDeque;
import com.mongodb.DBObject;

public class TickProducerConsumer<MESSAGE> extends SimpleProducerConsumer<MESSAGE> {
	private MongoTickDAO mtDao = null; 
	public long maxQlength = 0;
	public long queueCnt = 0;
	public long queueDrains = 0;

	public TickProducerConsumer(CircularArrayDeque<DBObject> messageQueue, CircularArrayDeque<DBObject> drainQueue) {
		super((Queue<MESSAGE>) messageQueue, (Queue<MESSAGE>) drainQueue);
	}

	/**
	 * Callback when there is data provided that needs to be consumed
	 */
	public void onConsume(Queue<MESSAGE> queue) {		
		if(queue.size() > maxQlength) {
			maxQlength = queue.size();
			System.out.println("onConsume:: New maxQSize= " + queue.size() + " maxQlength=" + maxQlength);
		}
		
		ArrayList<DBObject> msgArray = new ArrayList<DBObject>(); 
		Iterator<DBObject> it = (Iterator<DBObject>) queue.iterator();
		int j = 0;
		int localQ = queue.size();    // Grab size as it decreases to zero after all reads..
		
		/**
		 * Copy queue elements to msgArray[DBObject] for compatibility with 
		 * MongoDB bulk insert 
		 */
		while(it.hasNext()) {
			msgArray.add((DBObject) queue.poll());
			it.next();

			if(j == 0 )   // Debug fist 3 lines of each dequeue onConsume() call
				System.out.println("         ::circularArrayDeque:: size=" + queue.size() + " [" + j + "] val=[" + msgArray.get(j++) + "]" );
		}
		
		/**
		 * Print some statistics and perform a bulk insert into MongoDB
		 */
		if(msgArray.size() > 0) {

			this.queueCnt = this.queueCnt + Long.valueOf(msgArray.size());
			this.queueDrains++;
			System.out.println("         :: Drained queue.size(" + localQ 
					+ ") queueCnt(" + this.queueCnt + " queueDrains(" + this.queueDrains 
					+ ") msg/drain =[" + (this.queueCnt / this.queueDrains) + "]" );

			/**
			 * Perform a bulk insert into MongoDB
			 */
			this.mtDao.InsertTickDocumentList(msgArray);
		}
		else
			System.out.println("         :: WARNING!! detected queue size of zero!");	
	}

	public MongoTickDAO getMtDao() {
		return (MongoTickDAO) mtDao;
	}

	public void setMtDao(MongoTickDAO mdt) {
		this.mtDao = (MongoTickDAO) mdt;
	}
}
