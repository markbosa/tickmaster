package tickerTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/** 
 * Requirements specifications from John Tandau
 * 
 * Scenarios:
 *        Auto-balance
 *          Split the file into multiple files than can be pushed in parallel 
 *			and run multiple applications, one for each file.
 *			
 *          Use one application with multiple producer consumers and distribute
 *			the data from one file in a round-robin fashion to the ProdCons.
 *
 *  Prerequisite: Run TickFileSplitter.java which reads the 500 million record 
 *  TickData.txt file and breaks it into 50 files of 10 million records each.
 *  
 *  For each file in the list (up to 20 due to small space in MongoDB Cluster)
 *  we will send a filename and create a Java Thread to execute the insertion
 *  of the file contents into a MongoDB database.  Note that the CSV file MUST
 *  be transformed into Document format (BSON data) for MongoDB bulk insertion.
 *  
 * @author Mark
 *
 */
public class TickMultiFeed extends TextFileUtils {
	private String bigFileName = "";
	private ArrayList<String> fileList = new ArrayList<String>();
	private ArrayList<ThreadMan> threadList = new ArrayList<ThreadMan>();
	private CurrentAverageService cas = new CurrentAverageService();

	MongoDBU mdbu = null;

	TickMultiFeed( String bigDataDirectory) {
		super(bigDataDirectory);
		System.out.println("CurrentAverageService:: Elements do not exist: " + cas.cavHm.isEmpty());
	}

	/**
	 * Getter / Setter methods - self documenting
	 */
	public String getBigFileName() {
		return bigFileName;
	}
	public void setBigFileName(String bigFileName) {
		this.bigFileName = bigFileName;
	}

	/**
	 * Step 1: Connect to a single MongoDB connection
	 *         The Mongo Java Driver allows for up to 100 connections
	 *         with a single connector, so we we use this parent to
	 *         spawn the threads to access MongoDB.
	 */
	/**
	 * Step Two: Set up a MongoDB Connection which will be the persistent
	 * data store for the tick data.   Note that we use a wrapper class
	 * MongoDBU to specify the database name, port number, etc.
	 */
	private boolean getMongoConnector() {
		this.mdbu = new MongoDBU();
		boolean brc = this.mdbu.MongoInitialize(MongoDBU.TEST_CLUSTER_R1);
		return brc;
	}

    Date t1 = new Date();
	
	/**
	 * Step 2: gatherTextFiles() - 
	 * 
	 *  Build a list of text files that contain 10 million lines of input
	 *  data.  These files are the output of the TickFileSplitter.java 
	 *  program.  The size and number of files to produce can be modified
	 *  if you want smaller or larger input files.
	 *  Dependency:  The Directory MUST have a list of data files to process.
	 */
	private int gatherTexFiles() {
		File directory = new File(this.getDataDirectory());

		this.getTextFiles(directory, this.fileList);

		for(int k = 0; k < this.fileList.size(); k++) {			
			System.out.println(this.fileList.get(k).toString());
		}
		return this.fileList.size();		
	}
	
	/**
	 * Step 3: Call the ThreadManager (ThreadMan) for each of the input files.
	 *         This dispatches all threads to run in parallel
	 *         As symbols in the file span across files, we need a global 
	 *         service to handle all of the files at the same time.  
	 *         We will likely need synchronization or a queuing mechanism
	 *         to prevent re-entrantcy issues.
	 * @param offset  :: Allows us to load selected files, a good
	 *                   ETL practice to avoid duplicate data
	 * @return boolean true = if there were no Exceptions trapped
	 */
	private boolean dispatchThreads(int offset) {
		String testName = "";
		
		for(int k = 0; k < this.getNumberOfFiles(); k++) {			
			System.out.println("Dispatching File :" + this.fileList.get((k + offset)).toString());
			/**
			 * Initialize the thread with everything needed to execute an 
			 * insert of file record data into a MongoDB database.
			 */
			ThreadMan tm = new ThreadMan(this.fileList.get(k + offset).toString());	
			
			threadList.add(tm);
			
			testName = "MongoTest-" + (k + offset);
			tm.setName(testName);
	//		tm.setPriority( ((k + 5) % 10) + 1 );
			tm.setMdbu(this.mdbu);
			tm.settCas(this.cas);

			MongoTickDAO mtDao = new MongoTickDAO(this.mdbu.mdu);
			mtDao.setMongoTickDao(mtDao);			
			mtDao.ensureIndex();
			
			tm.setMtDao(mtDao);			
			tm.start();
		}
		return false;
	}
	
	
	/**
	 * Step 4: Wait for threads to complete and shutdown queues
	 */
	boolean shutDownThreads() {
		boolean rc = false;
		/**
		 *  Try once a minute per file before attempting shutdown
		 */
		long sleepTime = 0 ;	
		int shutdown = 0;

		while(shutdown < this.getNumberOfFiles()) {
			try {
				sleepTime = 60 * 1000;
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/**
			 * go through each of of the Threads and get shutdown status
			 */			
			shutdown = 0;   // reset each attempt as ALL must be shutdown
			
			for(int k = 0; k < this.getNumberOfFiles(); k++) {
				ThreadMan tMan = threadList.get(k);

				if(tMan.getMtDao().tickProducerConsumer.isShutdown() == true) {
					System.out.println("ShutDownThread[" + k + "][" 
							+ tMan.getName() + " isShutdown = " 
							+ tMan.getMtDao().tickProducerConsumer.isShutdown()
							+ " shutdownCnt = " + shutdown );

					shutdown++;
					rc = true;
				}
				else
					System.out.println("ShutDownThread[" + k + "]["
							+ tMan.getName() + "] isShutdown = " 
							+ tMan.getMtDao().tickProducerConsumer.isShutdown()
							+ " shutdownCnt = " + shutdown );
			}
		}
		return rc;
	}

	/**
	 * Main() should be limited to defining the filename to process and to
	 * report on the results.  
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		TickMultiFeed tmf = new TickMultiFeed("C:\\Dev\\ClusterTest\\data\\");

		if(tmf.getMongoConnector() == false) {
			System.out.println("Failed to connect to MongoDB database: Exiting now!");
			System.exit(-1);
		}
		
		tmf.setBigFileName("TickData");
		tmf.setLineCounter(0);
		tmf.setNumberOfFiles(1);

		int listSize = tmf.gatherTexFiles();
		System.out.println("DONE: Gathering " + listSize + " txt files.");
		
		/**
		 * 5 = offset as 1-4 have been loaded
		 */
		int firstFile = 0;
		boolean rc = tmf.dispatchThreads(firstFile);
		System.out.println("DONE: Dispatching " + tmf.getNumberOfFiles() + " Threads rc=" + rc + " Loading from file: " + firstFile);		
		
		boolean rc2 = tmf.shutDownThreads();
		System.out.println("DONE: Shutting Down Threads rc2=" + rc2 );	
		
		/**
		 * Now hit the CurrentAverageService and have it print the final
		 * statistics for each symbol.  
		 */
		tmf.cas.printHashMap();
	}
}
