package tickerTest;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import com.gmanalytics.algorithms.concurrent.SimpleProducerConsumer;
import com.gmanalytics.algorithms.structures.CircularArrayDeque;
import com.gmanalytics.symbology.persistence.impl.mongoDriver.MongoDba.CollectionIndex;
import com.gmanalytics.symbology.persistence.impl.mongoDriver.MongoDba.MongoDriverUtils;

public class MongoTickDAO extends CollectionIndex {
	    private final String collectionName = "ticks";
	    //int maxQueueSize = 2097151; 		// 2MB in bytes minus one	 
//	    int maxQueueSize = 1048576;         // 1MB in bytes
	    int maxQueueSize = 524288;         	// 1/2 MB (512 KB) in bytes

		protected CircularArrayDeque<DBObject> messageQueue	=  new CircularArrayDeque<DBObject>(maxQueueSize);
		protected CircularArrayDeque<DBObject> drainQueue	=  new CircularArrayDeque<DBObject>(maxQueueSize / 10);
		protected TickProducerConsumer<DBObject> tickProducerConsumer = null;

		public MongoTickDAO() {
	        super();
	        this.CreateSimpleProducerConsumer();
	    }

	    public MongoTickDAO(MongoDriverUtils mdu) {
	    	super(mdu);
			this.setDBCollection(this.mongoDriverUtil.getDb().getCollection(this.collectionName));
	        this.CreateSimpleProducerConsumer();
		}
	    
    	/**
    	 * Primary point to generate a bsonDoc from an input of a String array
    	 * of tick data items parsed from a file or data feed.
    	 * @param String[] tickVal - Values are optional so ALL values MUST
    	 *                 be null checked before adding to the bson document
    	 * @return true if no insert failures, false at the item that failed.
    	 */	    
	    public DBObject createTickDoc(String[] tickVal, long even) {

	 		/**
    		 * Create a BSON formatted document with image and text data
    		 */
    		DBObject bsonDoc = (DBObject) new BasicDBObject();
	    	
    		/**
    		 * First field is a mandatory "data center" tag to identity the 
    		 * nearest data center.   For testing this is just a flip-flop
    		 * between even and odd documents, but it is expected that the
    		 * web load-balancer will be able to identify the source of the
    		 * web request and pass the value as with "even"
    		 */
	    	if( (even % 2) == 0) {
	    		bsonDoc.put("dc",	"NY2");
	    	}
	    	else {
	      		bsonDoc.put("dc",	"NY4");
	    	}
    		
	    	if(tickVal[0].toString().compareTo("") != 0) {
	    		bsonDoc.put("fid",	tickVal[0]);
	    	}
	    	if(tickVal[1].toString().compareTo("") != 0) {
	    		bsonDoc.put("dts",	tickVal[1]);
	    	}
	    	if(tickVal[2].toString().compareTo("") != 0) {
	    		bsonDoc.put("status",tickVal[2]);
	    	}
	    	if(tickVal[3].toString().compareTo("") != 0) {
	    		bsonDoc.put("v1",	tickVal[3]);
	    	}
	    	if(tickVal[4].toString().compareTo("") != 0) {
	    		bsonDoc.put("v2",	tickVal[4]);
	    	}
	    	if(tickVal[5].toString().compareTo("") != 0) {
	    		bsonDoc.put("v3",	tickVal[5]);
	    	}
	    	if(tickVal[6].toString().compareTo("") != 0) {
	    		bsonDoc.put("v4",	tickVal[6]);
	    	}
	    	
	    	return bsonDoc;
	    }
	    
	    /** 
	     * @param A bsonDoc that contains the tick data 
	     * @return true if no insert failures, false at the item that failed.
    	 */	    
	    public boolean InsertTickDocument(DBObject bsonDoc) {
	 
	    	WriteResult rc = this.getDBCollection().insert(bsonDoc);

	    	if (rc.getError() != null) {
	    		System.out.println("MongoTickDAO::InsertTickDocument() Error: "
	    				+ rc.getError());
	    		return false;
	    	}
	    	return true;
	    }
	    
	    /** 
	     * @param An ArrayList of bsonDoc formatted documents that contain tick
	     *  		data - This format allows for a bulk insert into MongoDB 
	     * @return true if no insert failures, false at the item that failed.
    	 */	    
	    public boolean InsertTickDocumentList(ArrayList<DBObject> bsonDocList) {
	    	this.getDBCollection().insert(bsonDocList);
	    	return true;
	    }
	    
	    /** 
	     * @param A CircularArrayDeque of bsonDoc formatted documents that 
	     * 	contain tick data -This format allows for a bulk insert into MongoDB 
	     * @return true if no insert failures, false at the item that failed.
    	 */	    
	    public boolean InsertTickDocumentCircularList(DBObject[] dbArray) {
	 		    	
	    	this.getDBCollection().insert(dbArray, WriteConcern.ACKNOWLEDGED);
	    	
	 //   	this.getDBCollection().initializeOrderedBulkOperation()
	 //   	this.getDBCollection().
	    	return true;
	    }
	    
	    /**
	     * ensureIndex() - A method to call the Collection wrapper and create
	     * a compound index based on the data center and fid value 
	     */
	    public void ensureIndex() {
	    
		BasicDBObject index2 = new BasicDBObject();
		index2.put("dc", 1);
		index2.put("fid", 1);
		this.ensureIndex(index2);
	    }
	    /**
	     * Clear out the queue as the first task in initialization of the queue
	     */
	    public void CreateSimpleProducerConsumer( ) {
	    	this.messageQueue.clear();
	    	this.drainQueue.clear();
	    	this.tickProducerConsumer = new TickProducerConsumer<DBObject>(this.messageQueue, this.drainQueue);
	    }
	    	    
	    public boolean setMongoTickDao(MongoTickDAO mdt) {
	    	this.tickProducerConsumer.setMtDao(mdt);
	    	return true;
	    }
	    
	    public void AddToMessageQueue(DBObject bsonDoc) {	    	
	    	try {
	    		tickProducerConsumer.put(bsonDoc, 50L, TimeUnit.SECONDS);
//		    	tickProducerConsumer.add(bsonDoc);
	    	} catch (IllegalStateException | InterruptedException e) {
	    		System.out.println("MongoTickDAO::AddToMessageQueue Exception: " + e);
	    		e.printStackTrace();
	    	} catch (TimeoutException e) {
	    		System.out.println("MongoTickDAO::AddToMessageQueue TimoutException: " + e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    public long getMaxOnConsumeSize() {
	    	return tickProducerConsumer.maxQlength;
	    }	    
}
