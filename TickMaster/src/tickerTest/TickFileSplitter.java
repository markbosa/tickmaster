/**
 * 
 */
package tickerTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Date;

import com.gmanalytics.algorithms.io.CSVParserBuilder;
import com.gmanalytics.algorithms.io.CSVReader;
import com.gmanalytics.algorithms.io.CSVReader.LineTooLongException;
import com.gmanalytics.algorithms.io.CSVReader.ParseErrorException;
import com.mongodb.DBObject;

/**
 * @author Mark
 *
 */
public class TickFileSplitter extends TextFileUtils {

	/**
	 * Default Constructor
	 */
	public TickFileSplitter() {
		super( "C:\\Dev\\ClusterTest\\data\\", "TickData" );
	}	

	/**
	 * Getter and Setter Methods
	 */

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final int SINGLE_INSERT = 1;
		final int SIMPLE_PRODUCER_CONSUMER = 2;
		int mode = SINGLE_INSERT;

		/**
		 *  Step One: Identify and open reader for tick data file
		 *  This is the input source of data to be split into many 
		 *  smaller files limited to the numberOfLine parameter per file
		 *  
		 *  NOTE: Test tick data file has 500 million records
		 */
		TickFileSplitter tfs = new TickFileSplitter();
		tfs.setCsvFileName(tfs.getDataDirectory() + tfs.getCsvRootName() + ".txt");
		tfs.setNumberOfFiles(1);
		tfs.setNumberOfLines(10000000);

		/**
		 * Step Two: Set up a MongoDB Connection which will be the persistent
		 * data store for the tick data.   Note that we use a wrapper class
		 * MongoDBU to specify the database name, port number, etc.
		 */
		MongoDBU mdbu = new MongoDBU();
		mdbu.MongoInitialize(MongoDBU.LOCALHOST);
		MongoTickDAO mtDao = new MongoTickDAO(mdbu.mdu);
		mtDao.setMongoTickDao(mtDao);
		Date t1 = new Date();

		try {
			/**
			 * Step 3: Instantiate a TickFileSplitter class with the input file
			 */	
			InputStream fls = new FileInputStream(tfs.getCsvFileName());
			tfs.setTickReader( new BufferedReader(new InputStreamReader(fls, Charset.forName("UTF-8"))));

//			CSVParserBuilder csvParserBuilder = new CSVParserBuilder();
//			csvParserBuilder.addDelimiter('\t');  // tab separated data
//
//			CSVReader csvr = new CSVReader(tfs.getTickReader(), csvParserBuilder);
			String[] nextLine = null;
			

			//		    int data = reader.read();
			//		    int max = 0;
			//		    while(data != -1){
			//		        char dataChar = (char) data;
			//		        data = reader.read();
			////		        System.out.println("dataChar:" + dataChar + " data=[" + data + "]");
			//		        System.out.print(dataChar);
			//
			//		        if(max++ > 100)
			//		        	System.exit(1);
			//		    }

			/**
			 * Step 4: Create a file Writer for the split chunks of the input data
			 *         Key is to generate names appended by "01 through 50".txt 
			 */
			FileWriter fw = null;
			String inLine = new String();
			
			/**
			 * Switch on Step 5 to different test modes
			 */
			switch(mode) {	
			case SINGLE_INSERT :
				/**
				 * Step Four: Read one line at a time from the reader and then
				 * store that line as a new MongoDB document
				 */
//				nextLine = csvr.read();
			
				//				while( nextLine != null) {
				tfs.setOutFileName( tfs.getDataDirectory() + tfs.getCsvRootName() + "-" 
						+ String.format("%02d", tfs.getNumberOfFiles())  + ".txt" );

				fw = new FileWriter( tfs.getOutFileName());
				tfs.setTickWriter(new BufferedWriter(fw));
				
				while( (inLine = tfs.getTickReader().readLine()) != null) {
//        				System.out.println(tfr.lineCounter++ +"::");
//
//        				for(int i = 0; i < nextLine.length; i++) {
//        					//System.out.println("Token(" + i + ")=[" + nextLine[i] + "]");
//        					System.out.print( nextLine[i] + '\t');
//        					
//        				}
					/**
					 * write current line and read the next
					 */
					tfs.getTickWriter().write(inLine + "\n");
//					nextLine = csvr.read();
					
//					tickDoc = mtDao.createTickDoc(nextLine);
//					mtDao.InsertTickDocument(tickDoc);
					
					if(++tfs.lineCounter % tfs.getNumberOfLines() == 0) {
						Date t2 = new Date();
						long millis = t2.getTime() - t1.getTime();
						long seconds = (millis / 1000) % 60;
						long minutes = ((millis - seconds) / 1000) / 60;

						System.out.printf(
								"TickFileSplitter::Inserted %d Docs into file: %s at %d minutes, %d seconds %d millis \n", 
								tfs.lineCounter, tfs.getOutFileName() , minutes, seconds, millis);
						
						/**
						 * Close out the current file and create the next one.
						 */
						tfs.getTickWriter().close();						
						tfs.setNumberOfFiles(tfs.getNumberOfFiles() + 1);
						
						/**
						 * Now open the next output file
						 */
						tfs.setOutFileName( tfs.getDataDirectory() + tfs.getCsvRootName() + "-" 
								+ String.format("%02d", tfs.getNumberOfFiles())  + ".txt" );

						fw = new FileWriter( tfs.getOutFileName());
						tfs.setTickWriter(new BufferedWriter(fw));
					}
				}
				break;

				case SIMPLE_PRODUCER_CONSUMER:
					/**
					 * Step Four: Read one line at a time from the reader and then
					 * store that line as a new MongoDB document
					 */
//					nextLine = csvr.read();

					tfs.setOutFileName( tfs.getDataDirectory() + tfs.getCsvRootName() + "-" 
							+ String.format("%02d", tfs.getNumberOfFiles())  + ".txt" );												
		
					fw = new FileWriter( tfs.getOutFileName());
					tfs.setTickWriter(new BufferedWriter(fw));

					
					while( nextLine != null) {

//						System.out.println(tfr.lineCounter++ +"::");
//
//						for(int i = 0; i < nextLine.length; i++) {
//							//System.out.println("Token(" + i + ")=[" + nextLine[i] + "]");
//							System.out.print( nextLine[i] + '\t');
//
//						}
						if(++tfs.lineCounter % 1000000 == 0) {
							Date t2 = new Date();
							long millis = t2.getTime() - t1.getTime();
							long seconds = (millis / 1000) % 60;
							long minutes = ((millis - seconds) / 1000) / 60;

							System.out.printf(
									"TickFileSplitter::Inserted %d Docs in %d minutes, %d seconds %d millis \n", 
									tfs.lineCounter, minutes, seconds, millis);
						}

//						tickDoc = mtDao.createTickDoc(nextLine);
//						mtDao.AddToMessageQueue(tickDoc);
						break;
					}
//					nextLine = csvr.read();
				}

			} catch (FileNotFoundException e) {
				System.out.println( "FileNotFoundException at line::" + tfs.getLineCounter());
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println( "IOException at line::" + tfs.getLineCounter());
				e.printStackTrace();
//			} catch (ParseErrorException e) {
//				System.out.println( "ParseErrorException at line::" + tfs.getLineCounter());
//				e.printStackTrace();
//			} catch (LineTooLongException e) {
//				System.out.println( "LineTooLongException at line::" + tfs.getLineCounter());
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				System.out.println( "InterrruptedException at line::" + tfs.getLineCounter());
//				e.printStackTrace();
			} catch (Exception e) {
				System.out.println( "Exception at line::" + tfs.getLineCounter());
				e.printStackTrace();
			}

			Date t3 = new Date();
			long millis = t3.getTime() - t1.getTime();
			long seconds = (millis / 1000) % 60;
			long minutes = ((millis - seconds) / 1000) / 60;
			System.out.printf("TickFileSplitter:: Insert duration: %d minutes, %d seconds %d millis\n",
					minutes, seconds, millis);
			System.out.println("TickFileSplitter:: Inserted " + tfs.getLineCounter() 
					+ " Documents into MongoDB TickMaster.ticks collection\n");
			System.out.println("Maximum onConsume() size=" + mtDao.getMaxOnConsumeSize());
		}
	}
