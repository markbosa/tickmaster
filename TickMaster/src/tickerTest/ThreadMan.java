package tickerTest;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Date;

import com.gmanalytics.algorithms.io.CSVParserBuilder;
import com.gmanalytics.algorithms.io.CSVReader;
import com.gmanalytics.algorithms.io.CSVReader.LineTooLongException;
import com.gmanalytics.algorithms.io.CSVReader.ParseErrorException;
import com.mongodb.DBObject;

public class ThreadMan extends Thread {
	final int SINGLE_INSERT = 1;
	final int SIMPLE_PRODUCER_CONSUMER = 2;
	int mode = SIMPLE_PRODUCER_CONSUMER;
	private int lineCounter = 0;
	private String csvFileName = "";
	private Reader tickReader = null;
	private MongoDBU mdbu = null;
	MongoTickDAO mtDao = null;
	

	ThreadMan(String inputFile) {
		this.csvFileName = inputFile;
	}

	
	
	public Reader getTickReader() {
		return tickReader;
	}

	public void setTickReader(Reader tickReader) {
		this.tickReader = tickReader;
	}

	public MongoDBU getMdbu() {
		return mdbu;
	}

	public void setMdbu(MongoDBU mdbu) {
		this.mdbu = mdbu;
	}

	public void setMtDao(MongoTickDAO mtDaoIn) {
		this.mtDao = mtDaoIn;
	}

	public MongoTickDAO getMtDao() {
		return this.mtDao;
	}	
	
	public void run() {
		long sleepTime = this.getPriority() * 100;

		System.out.println("ThreadMan processing file: " + this.csvFileName 
				+ " PID: " + this.getId() + " Priority: " + this.getPriority() 
				+ " Thread Name: " + this.getName() + " SleepTime= " + sleepTime);

		/**
		 * Now do the real work
		 */
		/**
		 * Step 3: Instantiate a TickFileReader class with the input file
		 */	
		try {
			this.tickReader = new FileReader(this.csvFileName);

			CSVParserBuilder csvParserBuilder = new CSVParserBuilder();
			csvParserBuilder.addDelimiter('\t');  // tab separated data

			CSVReader csvr = new CSVReader(this.getTickReader(), csvParserBuilder);
			String[] nextLine = null;
			DBObject tickDoc = null;
			Date t1 = new Date();

			nextLine = csvr.read();
			
			while( nextLine != null) {
				//			System.out.println(tfr.lineCounter++ +"::");
				//
				//			for(int i = 0; i < nextLine.length; i++) {
				//				//System.out.println("Token(" + i + ")=[" + nextLine[i] + "]");
				//				System.out.print( nextLine[i] + '\t');
				//				
				//			}
				if(++this.lineCounter % 1000000 == 0) {
					Date t2 = new Date();
					long millis = t2.getTime() - t1.getTime();
					long seconds = (millis / 1000) % 60;
					long minutes = ((millis - seconds) / 1000) / 60;

					System.out.printf(
							"TickFileReader::Inserted %d Docs in %d minutes, %d seconds %d millis \n", 
							this.lineCounter, minutes, seconds, millis);
				}

				tickDoc = mtDao.createTickDoc(nextLine);

				switch(mode) {
				case SINGLE_INSERT :
					mtDao.InsertTickDocument(tickDoc);
					break;

				case SIMPLE_PRODUCER_CONSUMER:
					mtDao.AddToMessageQueue(tickDoc);
					break;
				}
				nextLine = csvr.read();
			}
			nextLine = csvr.read();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineTooLongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * Diagnostics
		 */
//		try {
//			Thread.sleep(sleepTime);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("ThreadMan processing file: " + this.fileName 
//				+ " PID: " + this.getId() + " Priority: " + this.getPriority() 
//				+ " Thread Name: " + this.getName() + " Awoke from SleepTime= " + sleepTime);
	}


	public static void main(String[] args) {

		String testName = "";

		for(int i = 0; i < 10; i++) {
			testName = "test" + i;
			ThreadMan tm = new ThreadMan(testName);
			tm.setName(testName);
			tm.setPriority(10 -i);
			tm.start();
		}
	}
}
