package tickerTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class TextFileUtils {
	protected long lineCounter = 0;
	private long numberOfFiles = 0; // number of files to create
	private long numberOfLines = 0;	// number of lines in each split file
	private String dataDirectory = "";
	private String csvFileName = null;
	private String csvRootName = "";
	private String outFileName = null;  // This will be the input file name plus a number
	private BufferedReader tickReader = null;
	private BufferedWriter tickWriter = null;

	public TextFileUtils(String defaultDataDirectory) {
		this.dataDirectory = defaultDataDirectory; // C:\\Dev\\ClusterTest\\data\\
	}
	
	public TextFileUtils(String defaultDataDirectory, String rootName) {
		this.dataDirectory = defaultDataDirectory; // C:\\Dev\\ClusterTest\\data\\
		this.setCsvRootName(rootName);              // "TickData"
	}

	/**
	 * Getter and Setter methods for TextFileUtils class
	 */
	public String getDataDirectory() {
		return dataDirectory;
	}

	public void setDataDirectory(String dataDirectory) {
		System.out.println("Default data directory set to: " + dataDirectory);
		this.dataDirectory = dataDirectory;
	}

	/**
	 * Getter and Setter Methods
	 */
	public BufferedReader getTickReader() {
		return tickReader;
	}

	public void setTickReader(BufferedReader inputStreamReader) {
		this.tickReader = inputStreamReader;
	}

	public long getLineCounter() {
		return lineCounter;
	}

	public void setLineCounter(long lineCounter) {
		this.lineCounter = lineCounter;
	}

	public String getCsvFileName() {
		return csvFileName;
	}

	public void setCsvFileName(String csvFileName) {
		this.csvFileName = csvFileName;
	}

	public String getCsvRootName() {
		return csvRootName;
	}

	public void setCsvRootName(String csvRootName) {
		this.csvRootName = csvRootName;
	}

	public long getNumberOfLines() {
		return numberOfLines;
	}

	public void setNumberOfLines(long numberOfLines) {
		this.numberOfLines = numberOfLines;
	}

	public long getNumberOfFiles() {
		return numberOfFiles;
	}

	public void setNumberOfFiles(long numberOfFiles) {
		this.numberOfFiles = numberOfFiles;
	}

	public Writer getTickWriter() {
		return tickWriter;
	}

	public void setTickWriter(BufferedWriter fw) {
		this.tickWriter = fw;
	}

	public String getOutFileName() {
		return outFileName;
	}

	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	public BufferedReader getDataFiles(String relativePath, String fileName) {
		BufferedReader reader;

		Path tmpPath = FileSystems.getDefault().getPath(relativePath, fileName);

		try {
			reader = Files.newBufferedReader(tmpPath, StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return reader;
	}

	/**
	 * Filter out the data files 
	 */
	class Filter implements FileFilter  
	{  
		public boolean accept(File file)  
		{  
			return file.getName().endsWith("txt");  
		}  
	}  

	/**
	 * getTextFiles() - From a base root directory, gather recursively all of the
	 *               text files you can find. 
	 * @param  Directory - A String representing a Linux directory
	 * @param  fileList  - A reference to a growing list of text (*.txt) files
	 * @return fileList reference is returned (may not bee needed)
	 */	
	public ArrayList<String> getTextFiles(File Directory, 	ArrayList<String> fileList) {
		int textFileCnt = 0;
		/*
		 * Go into each directory and gather text files 
		 */
		File[] dataDir = Directory.listFiles();  

		for (int index = 0; index < dataDir.length; index++)  
		{  
			/* 
			 * Print out the name of files in the directory however filter out 
			 * the DS_Store hidden files which appear to be an artifact of the 
			 * Mac-OS-X output  
			 */
			if( dataDir[index].toString().endsWith("DS_Store")) {
				continue;
			}
			/**
			 * Check if directory and if so we recursively call getTextFiles
			 * to get any text images
			 */
			else if(dataDir[index].isDirectory() == true) {
				/**
				 * If this is a directory, recursively call this routine to 
				 * get the jpeg image files we are searching for
				 */
				this.getTextFiles(dataDir[index], fileList);
			}
			else if(dataDir[index].isFile() == true) {
				/**
				 * Only collect text files (*.txt)
				 */
				if( dataDir[index].toString().endsWith("txt") &&
					dataDir[index].toString().contains("-")) {
//					System.out.println("Processing text File:: " + dataDir[index].toString());
					fileList.add(dataDir[index].toString());
					textFileCnt++;
				}
			}
		}
		if(textFileCnt > 0) 
			System.out.println("Found[" + textFileCnt + "] text files");
		return fileList;
	}


	public void FilePathExample() 
	{
		try {

			String filename = "testing.txt";
			String finalfile = "";
			String workingDir = System.getProperty("user.dir");

			String your_os = System.getProperty("os.name").toLowerCase();
			if(your_os.indexOf("win") >= 0){
				finalfile = workingDir + "\\" + filename;
			}else if(your_os.indexOf( "nix") >=0 || your_os.indexOf( "nux") >=0){
				finalfile = workingDir + "/" + filename;
			}else{
				finalfile = workingDir + "{others}" + filename;
			}

			System.out.println("Final filepath : " + finalfile);
			File file = new File(finalfile);

			if (file.createNewFile()){
				System.out.println("Done");
			}
			else{
				System.out.println("File already exists!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * main() - Test program to gather text files
	 * @param 1 - The full directory name containing text files
	 */
	public static void main(String[] args) {
		/**
		 * Use constructor to set the root data directory
		 */
		TextFileUtils ofu = new TextFileUtils(
				"/home/markbosa/NRDA_Image_Library", "TickData");

		File directory = new File(ofu.getDataDirectory());
		ArrayList<String> imageFileList = new ArrayList<String>();
		ofu.getTextFiles(directory, imageFileList);

		for(int k = 0; k < imageFileList.size(); k++) {			
			System.out.println(imageFileList.get(k).toString());
		}		
		System.out.println("DONE: Gathering " + imageFileList.size() + " text files.");
	}
}
